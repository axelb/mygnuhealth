#!/usr/bin/env python3
##############################################################################
#
#    MyGNUHealth : Mobile and Desktop PHR node for GNU Health
#
#           MyGNUHealth is part of the GNU Health project
#
##############################################################################

__version__ = "0.7a11"

__appname__ = "MyGNUHealth"

__description__ = "A Personal Health Information Management System"

__homepage__ = "https://www.gnuhealth.org"

__author__ = "GNU Solidario"

__email__ = "info@gnuhealth.org"

__download_url__ = 'https://ftp.gnu.org/gnu/health'

__team__ = [
    {"name": "Luis Falcon", "email": "falcon@gnuhealth.org",
     "tasks": "Original author and developer"},
    {"name": "Carl Schwan", "email": "carlschwan@kde.org",
     "tasks": "Kirigami, QML"},
    {"name": "Aleix Pol", "email": "aleixpol@kde.org",
     "tasks": "Kirigami, packaging"},
    {"name": "Cristian Maureira", "email": "Cristian.Maureira-Fredes@qt.io",
     "tasks": "PySide"},
    ]

__license__ = "GPL v3+"
__copyright__ = "Copyright 2008-2020 {0}".format(__author__)
